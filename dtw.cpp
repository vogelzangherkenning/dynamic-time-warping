#include "include/algo.hpp"
#include "include/dtw.hpp"
#include "include/policies.hpp"
#include "include/printers.hpp"
#include "include/reducers.hpp"
#include "include/util.hpp"

#include <fstream>      // for std::ifstream
#include <iostream>     // for std::cout
#include <vector>       // for std::vector

/*
 * Normalization of input data will happen NORMALIZATION_ROUNDS times
*/
#define NORMALIZATION_ROUNDS 0

/*
 * Define PRDAT_INPUT_ARGS if you want to specify
 * input files as arguments to the application
*/
//#define PRDAT_INPUT_ARGS

int main(int argc, char** argv)
{

#if defined(PRDAT_INPUT_ARGS)
  if(argc < 3) {
    std::cout << "Usage: dtw <samplefile> <queryfile>" << std::endl;
    std::exit(0);
  }
#else
  argv[1] = (char*)"lc.txt";
  argv[2] = (char*)"lq.txt";
#endif

  std::vector<double> q;
  std::vector<double> c;

  { // Open input files in this scope so they
    // get closed after we are done with them
    std::ifstream fq(argv[2]);
    std::ifstream fc(argv[1]);

    if(!fq.is_open()) {
      std::cerr << "aborting: Could not open Q input file"
                << std::endl;
      std::exit(1);
    }

    if(!fc.is_open()) {
      std::cerr << "aborting: Could not open C input file"
                << std::endl;
      std::exit(2);
    }

    //util::istream_fill(fq, q);
    //util::istream_fill(fc, c);
    util::r_istream_fill(fq, q);
    util::r_istream_fill(fc, c);
  }

  // Q's size must not be larger than C's size
  if(q.size() > c.size()) {
    std::cerr << "aborting: Q.size() > C.size()"
              << std::endl;
    std::exit(3);
  }

  // Normalize our data
  for(int i = 0; i < NORMALIZATION_ROUNDS; ++i) {
    q = dtw::normalize(q);
    c = dtw::normalize(c);
  }

  // Reduce dimensionality using PAA
  q = dtw::reduce(q, make_paa(16, q));
  c = dtw::reduce(c, make_paa(16, c));

  std::cout << "Reduced data:"
            << std::endl;
  std::cout << "Q: "
            << pretty_print(q)
            << std::endl;
  std::cout << "C: "
            << pretty_print(c)
            << std::endl;

  std::cout << std::endl;

  std::cout << "Lemire distance: "
            << dtw::lowerbound(q, c, algo::lemire(3))
            << std::endl;

  std::cout << std::endl;

  // Calculate our warping matrix
  dtw::warpingmatrix<double> mat = dtw::get_warpingmatrix(q, c);
  std::cout << "Warping matrix:"
            << std::endl;
  std::cout << pretty_print(mat)
            << std::endl;

  std::cout << std::endl;

  // Get warping path using Sakoe-Chiba band with a width of 3
  dtw::warpingpath path = dtw::get_warpingpath(mat, make_sakoechiba<EastSouthEast>(3, mat));
  std::cout << "Warping path:"
            << std::endl;
  std::cout << pretty_print(path, mat)
            << std::endl;
  std::cout << "- cost: "
            << dtw::get_path_cost(path, mat)
            << std::endl;
  std::cout << "- steps: "
            << path.size()
            << std::endl;

  return 0;
}
