#ifndef PRDAT_DATABASE_HPP_
#define PRDAT_DATABASE_HPP_

#include "include/column.hpp"
#include "include/field.hpp"
#include "include/primary_key.hpp"
#include "include/table.hpp"
#include "include/mpl/identity.hpp"
#include "include/mpl/type_sequence.hpp"

namespace birdshaz
{

namespace db
{

namespace strings
{

constexpr static char const birds[] = "BIRDS";
constexpr static char const birdsamples[] = "BIRDSAMPLES";

constexpr static char const birdid[] = "BIRDID";
constexpr static char const birdname[] = "BIRDNAME";
constexpr static char const family[] = "FAMILY";
constexpr static char const genus[] = "GENUS";
constexpr static char const species[] = "SPECIES";

constexpr static char const sampleid[] = "SAMPLEID";
constexpr static char const sampletype[] = "SAMPLETYPE";
constexpr static char const samplepath[] = "SAMPLEPATH";

} // namespace strings

struct birds
{
  constexpr static fp::table<birds, strings::birds> table = { };

  constexpr static fp::column<birds, strings::birdid, fp::field<int>> id = { };
  constexpr static fp::column<birds, strings::birdname, fp::field<char[255]>> name = { };
  constexpr static fp::column<birds, strings::family, fp::field<char[255]>> family = { };
  constexpr static fp::column<birds, strings::genus, fp::field<char[255]>> genus = { };
  constexpr static fp::column<birds, strings::species, fp::field<char[255]>> species = { };
}; // struct birds

constexpr fp::table<birds, strings::birds> birds::table;

constexpr fp::column<birds, strings::birdid, fp::field<int>> birds::id;
constexpr fp::column<birds, strings::birdname, fp::field<char[255]>> birds::name;
constexpr fp::column<birds, strings::family, fp::field<char[255]>> birds::family;
constexpr fp::column<birds, strings::genus, fp::field<char[255]>> birds::genus;
constexpr fp::column<birds, strings::species, fp::field<char[255]>> birds::species;

struct birdsamples
{
  constexpr static fp::table<birdsamples, strings::birdsamples> table = { };

  constexpr static fp::column<birdsamples, strings::sampleid, fp::field<int>> id = { };
  constexpr static fp::column<birdsamples, strings::birdid, fp::field<int>> bird = { };
  constexpr static fp::column<birdsamples, strings::sampletype, fp::field<char[5]>> type = { };
  constexpr static fp::column<birdsamples, strings::samplepath, fp::field<char[255]>> path = { };
}; // struct birdsamples

constexpr fp::table<birdsamples, strings::birdsamples> birdsamples::table;

constexpr fp::column<birdsamples, strings::sampleid, fp::field<int>> birdsamples::id;
constexpr fp::column<birdsamples, strings::birdid, fp::field<int>> birdsamples::bird;
constexpr fp::column<birdsamples, strings::sampletype, fp::field<char[5]>> birdsamples::type;
constexpr fp::column<birdsamples, strings::samplepath, fp::field<char[255]>> birdsamples::path;

} // namespace db

} // namespace birdshaz

#endif // PRDAT_DATABASE_HPP_
