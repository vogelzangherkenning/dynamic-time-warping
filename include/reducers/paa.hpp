#ifndef PRDAT_PAA_HPP_
#define PRDAT_PAA_HPP_

#include "../iterators/stateful_iterator.hpp"

#include <algorithm>
#include <cmath>
#include <iterator>

namespace dtw
{

namespace detail
{

template<typename Owner>
struct paa_state
{
public:
  typedef forward_iterator<double,
                           std::ptrdiff_t,
                           double,
                           double> base_type;

  typedef typename base_type::iterator_category iterator_category;
  typedef typename base_type::value_type value_type;
  typedef typename base_type::reference reference;
  typedef typename base_type::pointer pointer;
  typedef typename base_type::difference_type difference_type;

public:
  paa_state(Owner& owner, int dimension)
  : owner_(&owner)
  , dimension_(dimension)
  { }

  paa_state operator++(int)
  {
    paa_state copy = *this;
    ++dimension_;
    return copy;
  }

  paa_state& operator++()
  {
    ++dimension_;
    return *this;
  }

  paa_state operator--(int)
  {
    paa_state copy = *this;
    --dimension_;
    return copy;
  }

  paa_state& operator--()
  {
    --dimension_;
    return *this;
  }

  double operator*()
  {
    return owner_->reduce(dimension_);
  }

  bool operator==(const paa_state& other) const
  {
    return (dimension_ == other.dimension_);
  }
private:
  Owner* owner_;
  int dimension_;
};

}

template<typename Sequence>
class PAA
{
public:
  typedef PAA this_type;
  typedef Sequence sequence_type;
  typedef std::size_t size_type;
  typedef detail::paa_state<PAA> state_type;

  typedef double value_type;

  typedef stateful_iterator<state_type> iterator;
  typedef std::reverse_iterator<iterator> reverse_iterator;

public:
  PAA(const sequence_type& sequence, size_type dimensions)
  : sequence_(sequence)
  , dimensions_(dimensions)
  { }

  iterator begin()
  {
    return iterator(state_type(*this, 0));
  }

  iterator end()
  {
    return iterator(state_type(*this, dimensions_));
  }

  reverse_iterator rbegin()
  {
    return reverse_iterator(end());
  }

  reverse_iterator rend()
  {
    return reverse_iterator(begin());
  }

  value_type reduce(size_type dimension)
  {
    typedef typename sequence_type::const_iterator const_iterator;

    const double increment = (double)sequence_.size() / (double)dimensions_;

    const size_type idx_front = std::round(dimension * increment);
    const size_type idx_back = std::round((dimension + 1) * increment);
    // Prevent division by zero by returning early if front == back
    if(idx_front == idx_back) {
      return 0;
    }

    const_iterator first = std::next(sequence_.begin(), idx_front);
    const_iterator last = std::next(sequence_.begin(), idx_back);
    return std::accumulate(first, last, 0.0) / (idx_back - idx_front);
  }

private:
  const sequence_type& sequence_;
  size_type dimensions_;
};

}

template<typename Sequence>
dtw::PAA<Sequence> make_paa(std::size_t dim, const Sequence& seq)
{
  return dtw::PAA<Sequence>(seq, dim);
}

#endif
