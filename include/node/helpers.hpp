#include <node.h>
#include <v8.h>
#include <vector>

using namespace v8;

/**
 * Convert v8 array handle to vector
 */
std::vector<double> arrayToDoubleVector( Handle<Array> array ) {
	std::vector<double> vector;

	for( uint32_t i=0; i<array->Length(); i++ ) {
		vector.push_back( array->Get(i)->NumberValue() );
	}

	return vector;
}

/**
 * Convert vector to v8 array handle
 */
Handle<Array> doubleVectorToArray( std::vector<double> vector ) {
	Handle<Array> arr = Array::New(vector.size());
	uint32_t pos = 0;

	for( std::vector<double>::iterator it = vector.begin(); it != vector.end(); ++it )
    	arr->Set(pos++,Number::New(*it));

    return arr;
}