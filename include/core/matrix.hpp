#ifndef PRDAT_CORE_MATRIX_HPP_
#define PRDAT_CORE_MATRIX_HPP_

#include <algorithm>    // for std::fill
#include <vector>       // for std::vector

namespace core
{

template<typename>
class matrix;

template<typename Matrix>
class matrixrow
{
public:
  typedef matrixrow this_type;
  typedef Matrix matrix_type;
  typedef typename matrix_type::value_type value_type;
  typedef typename matrix_type::reference reference;
  typedef typename matrix_type::size_type size_type;

public:
  matrixrow(matrix_type& mat, size_type row)
  : matrix_(mat)
  , row_(row)
  { }

  reference operator[](int);
  value_type operator[](int) const;

private:
  matrix_type& matrix_;
  size_type row_;
};

template<typename T>
class matrix {
public:
  typedef matrix this_type;
  typedef matrixrow<matrix> row;

  typedef T value_type;
  typedef T& reference;
  typedef std::size_t size_type;

public:
  matrix(size_type rows, size_type columns)
  : data_(rows * columns)
  , rows_(rows)
  , columns_(columns)
  { }

  void clear(value_type x = value_type())
  {
    std::fill(data_.begin(), data_.end(), x);
  }

  row operator[](int r)
  { return row(*this, r); }

  row operator[](int r) const
  { return row(const_cast<matrix&>(*this), r); }

  reference get(size_type r, size_type c)
  { return data_[(r * columns_) + c]; }

  value_type get(size_type r, size_type c) const
  { return data_[(r * columns_) + c]; }

  size_type rows() const
  { return rows_; }

  size_type columns() const
  { return columns_; }

private:
  std::vector<T> data_;
  size_type rows_;
  size_type columns_;
};

template<typename T>
typename matrixrow<T>::reference matrixrow<T>::operator[](int i)
{
  return matrix_.get(row_, i);
}

template<typename T>
typename matrixrow<T>::value_type matrixrow<T>::operator[](int i) const
{
  return matrix_.get(row_, i);
}

}

#endif
