#ifndef PRDAT_UTIL_RANDOMFILL_HPP_
#define PRDAT_UTIL_RANDOMFILL_HPP_

#include <algorithm>    // for std::generate
#include <functional>   // for std::bind
#include <random>       // for std::mt19937, std::random_device,
                        // std::uniform_real_distribution

namespace util
{

template<typename Sequence>
static void random_fill(Sequence& seq)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(0, 100);

  std::generate(seq.begin(), seq.end(), std::bind(dist, gen));
}

}

#endif
