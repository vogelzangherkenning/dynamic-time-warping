#ifndef PRDAT_UTIL_MATH_HPP_
#define PRDAT_UTIL_MATH_HPP_

#include <type_traits>  // for std::common_type

namespace math
{

template<typename T, typename U>
inline bool eq(T l, U r)
{
  typedef typename std::common_type<T, U>::type common_type;
  return (static_cast<common_type>(l) == static_cast<common_type>(r));
}

template<typename T, typename U>
inline bool lt(T l, U r)
{
  typedef typename std::common_type<T, U>::type common_type;
  return (static_cast<common_type>(l) < static_cast<common_type>(r));
}

template<typename T, typename U>
inline bool gt(T l, U r)
{
  typedef typename std::common_type<T, U>::type common_type;
  return (static_cast<common_type>(l) > static_cast<common_type>(r));
}

template<typename T, typename U>
inline bool lte(T l, U r)
{
  typedef typename std::common_type<T, U>::type common_type;
  return (static_cast<common_type>(l) <= static_cast<common_type>(r));
}

template<typename T, typename U>
inline bool gte(T l, U r)
{
  typedef typename std::common_type<T, U>::type common_type;
  return (static_cast<common_type>(l) >= static_cast<common_type>(r));
}

}

#endif
