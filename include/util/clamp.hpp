#ifndef PRDAT_UTIL_CLAMP_HPP_
#define PRDAT_UTIL_CLAMP_HPP_

#include "identity.hpp"

#include <functional>   // for std::less
#include <iterator>     // for std::iterator_traits

namespace util
{

template<typename T, typename Pred>
inline T clamp(T x,
               typename identity<T>::type lo,
               typename identity<T>::type hi,
               Pred p)
{
  return p(x, lo) ? lo : p(hi, x) ? hi : x;
}

template<typename T>
inline T clamp(T x,
               typename identity<T>::type lo,
               typename identity<T>::type hi)
{
  return clamp(x, lo, hi, std::less<T>());
}

template<typename InputIter, typename OutputIter>
OutputIter clamp_range(InputIter first,
                       InputIter last,
                       OutputIter out,
                       typename std::iterator_traits<InputIter>::value_type lo,
                       typename std::iterator_traits<InputIter>::value_type hi)
{
  while(first != last) {
    *out++ = clamp(*first++, lo, hi);
  }
}

}

#endif
