#ifndef PRDAT_UTIL_IDENTITY_HPP_
#define PRDAT_UTIL_IDENTITY_HPP_

namespace util
{

template<typename T>
struct identity { typedef T type; };

}

#endif
