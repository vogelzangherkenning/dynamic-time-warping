#ifndef PRDAT_DTW_WARPINGMATRIX_HPP_
#define PRDAT_DTW_WARPINGMATRIX_HPP_

#include "../core/matrix.hpp"

namespace dtw
{

template<typename T>
using warpingmatrix = core::matrix<T>;

template<typename QSequence, typename CSequence>
warpingmatrix<double> get_warpingmatrix(const QSequence& Q, const CSequence& C)
{
  typedef typename warpingmatrix<double>::size_type size_type;

  warpingmatrix<double> mat(Q.size(), C.size());
  for(size_type r = 0; r < Q.size(); ++r) {
    for(size_type c = 0; c < C.size(); ++c) {
      mat[r][c] = std::abs(C[c] - Q[r]);
    }
  }
  return mat;
}

}

#endif
