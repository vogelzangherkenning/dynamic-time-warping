#ifndef PRDAT_REDUCE_HPP_
#define PRDAT_REDUCE_HPP_

namespace dtw
{

template<typename Sequence, typename Reducer>
Sequence reduce(const Sequence& seq, Reducer red)
{
  return Sequence(red.begin(), red.end());
}

}

#endif
