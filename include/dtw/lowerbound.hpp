#ifndef PRDAT_DTW_LOWERBOUND_HPP_
#define PRDAT_DTW_LOWERBOUND_HPP_

namespace dtw
{

template<typename Sequence, typename Func>
inline double lowerbound(const Sequence& Q, const Sequence& C, Func fn)
{
  return fn(Q, C);
}

}

#endif
