#ifndef PRDAT_PRINTERS_MATRIX_HPP_
#define PRDAT_PRINTERS_MATRIX_HPP_

#include <iomanip>    // for std::setw
#include <iostream>   // for std::ostream

namespace printers
{

template<typename Matrix>
class matrix_printer
{
public:
  typedef matrix_printer this_type;
  typedef Matrix matrix_type;

public:
  matrix_printer(const matrix_type& matrix)
  : matrix_(matrix)
  { }

public:
  friend std::ostream& operator<<(std::ostream& out, const matrix_printer& self)
  {
    typedef typename matrix_type::size_type size_type;
    for(size_type r = 0; r < self.matrix_.rows(); ++r) {
      for(size_type c = 0; c < self.matrix_.columns(); ++c) {
        out << std::setw(10) << self.matrix_.get(r, c);
      }
      if(r < (self.matrix_.rows() - 1)) out << std::endl;
    }
    return out;
  }

private:
  const matrix_type& matrix_;
};

}

template<typename Matrix, typename = typename Matrix::row>
printers::matrix_printer<Matrix> pretty_print(const Matrix& matrix)
{
  return printers::matrix_printer<Matrix>(matrix);
}

#endif
