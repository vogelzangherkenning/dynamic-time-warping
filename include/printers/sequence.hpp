#ifndef PRDAT_PRINTERS_VECTOR_HPP_
#define PRDAT_PRINTERS_VECTOR_HPP_

#include "../iterators/fixed_ostream_iterator.hpp"

#include <iostream>   // for std::ostream

namespace printers
{

template<typename Sequence>
class sequence_printer
{
public:
  typedef sequence_printer this_type;
  typedef Sequence sequence_type;

public:
  sequence_printer(const sequence_type& sequence)
  : sequence_(sequence)
  { }

public:
  friend std::ostream& operator<<(std::ostream& out,
                                  const sequence_printer& self)
  {
    using std::begin; using std::end;
    std::copy(begin(self.sequence_),
              end(self.sequence_),
              fixed_ostream_iterator(out, 10));

    return out;
  }

private:
  const sequence_type& sequence_;
};

}

template<typename Sequence, typename = typename Sequence::const_iterator>
printers::sequence_printer<Sequence> pretty_print(const Sequence& seq)
{
  return printers::sequence_printer<Sequence>(seq);
}

#endif
