#ifndef PRDAT_ITERATORS_STATEFUL_ITERATOR_HPP_
#define PRDAT_ITERATORS_STATEFUL_ITERATOR_HPP_

template<typename State>
class stateful_iterator
{
public:
  typedef stateful_iterator this_type;
  typedef State state_type;

  typedef typename State::value_type value_type;
  typedef typename State::reference reference;
  typedef typename State::pointer pointer;
  typedef typename State::difference_type difference_type;
  typedef typename State::iterator_category iterator_category;

public:
  explicit stateful_iterator(state_type state)
  : state_(state)
  { }

  reference operator*()
  {
    return *state_;
  }

  pointer operator->()
  {
    return &(operator*());
  }

  stateful_iterator operator++(int)
  {
    stateful_iterator copy = *this;
    state_ = next(state_);
    return copy;
  }

  stateful_iterator& operator++()
  {
    state_ = next(state_);
    return *this;
  }

  stateful_iterator operator--(int)
  {
    stateful_iterator copy = *this;
    state_ = prev(state_);
    return copy;
  }

  stateful_iterator& operator--()
  {
    state_ = prev(state_);
    return *this;
  }

  bool operator==(const stateful_iterator& other) const
  {
    return (state_ == other.state_);
  }

  bool operator!=(const stateful_iterator& other) const
  {
    return !(*this == other);
  }

private:
  state_type state_;
};

#endif
