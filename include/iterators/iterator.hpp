#ifndef PRDAT_ITERATORS_ITERATOR_HPP_
#define PRDAT_ITERATORS_ITERATOR_HPP_

#include <iterator>     // for std::iterator

struct output_iterator
{
  typedef void value_type;
  typedef void reference;
  typedef void pointer;
  typedef void difference_type;
  typedef std::output_iterator_tag iterator_category;
};

template<typename ValueType,
         typename Difference = std::ptrdiff_t,
         typename Pointer = ValueType*,
         typename Reference = ValueType&>
struct forward_iterator
{
  typedef ValueType value_type;
  typedef Reference reference;
  typedef Pointer pointer;
  typedef Difference difference_type;
  typedef std::forward_iterator_tag iterator_category;
};

template<typename ValueType,
         typename Difference = std::ptrdiff_t,
         typename Pointer = ValueType*,
         typename Reference = ValueType&>
struct bidirectional_iterator {
  typedef ValueType value_type;
  typedef Reference reference;
  typedef Pointer pointer;
  typedef Difference difference_type;
  typedef std::bidirectional_iterator_tag iterator_category;
};

#endif
