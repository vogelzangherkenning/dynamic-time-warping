#ifndef PRDAT_ITERATORS_SETW_HPP_
#define PRDAT_ITERATORS_SETW_HPP_

#include <iomanip>
#include <iostream>
#include <iterator>

struct fixed_ostream_iterator
: std::iterator<std::output_iterator_tag, void, void, void, void>
{
public:
  fixed_ostream_iterator(std::ostream& out, int width)
  : out_(&out)
  , width_(width)
  { }

  fixed_ostream_iterator operator++(int)
  {
    return *this;
  }

  fixed_ostream_iterator& operator++()
  {
    return *this;
  }

  fixed_ostream_iterator& operator*()
  {
    return *this;
  }

  template<typename T>
  fixed_ostream_iterator& operator=(const T& x)
  {
    *out_ << std::setw(width_) << x;
    return *this;
  }
private:
  std::ostream* out_;
  int width_;
};

#endif
