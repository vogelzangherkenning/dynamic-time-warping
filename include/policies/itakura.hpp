#ifndef POLICY_ITAKURA_HPP_
#define POLICY_ITAKURA_HPP_

#include "../iterators/step_iterator.hpp"

#include <cmath>      // for std::abs

template<template<typename> class StepPolicy, typename Matrix>
struct Itakura
{
public:
  typedef Itakura this_type;
  typedef std::size_t size_type;
  typedef StepPolicy<Itakura> step_policy;
  typedef typename step_policy::step_type step_type;
  typedef step_iterator<this_type> iterator;
  typedef reverse_step_iterator<this_type> reverse_iterator;

public:
  Itakura(size_type size, const Matrix& matrix)
  : size_(size)
  , matrix_(matrix)
  { }

  iterator begin()
  {
    return iterator(*this, step_policy::begin(matrix_));
  }

  iterator end()
  {
    return iterator(*this, step_policy::end(matrix_));
  }

  reverse_iterator rbegin()
  {
    return reverse_iterator(*this, step_policy::rbegin(matrix_));
  }

  reverse_iterator rend()
  {
    return reverse_iterator(*this, step_policy::rend(matrix_));
  }

  step_type next(const step_type& cur) const
  {
    const step_type next = step_policy::next(cur, matrix_, *this);
    if(step_policy::validate(next)) {
      return step_type(cur.first + next.first, cur.second + next.second);
    }
    return step_policy::end(matrix_);
  }

  step_type prev(const step_type& cur) const
  {
    const step_type prev = step_policy::prev(cur, matrix_, *this);
    if(step_policy::validate(prev)) {
      return step_type(cur.first + prev.first, cur.second + prev.second);
    }
    return step_policy::rend(matrix_);
  }

  bool validate(const step_type& cur, const step_type& next) const
  {
    const double ratio = (matrix_.columns() / matrix_.rows());
    const double middle = ratio * next.first;
    return ((next.first < matrix_.rows()) &&
           (next.second < matrix_.columns())) &&
           (std::abs(middle - next.first) <= size_);
  }

private:
  size_type size_;
  const Matrix& matrix_;
};

template<template<typename> class StepPolicy, typename Matrix>
Itakura<StepPolicy, Matrix> make_itakura(std::size_t n, const Matrix& matrix)
{
  return Itakura<StepPolicy, Matrix>(n, matrix);
}

#endif
