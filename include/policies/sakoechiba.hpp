#ifndef POLICY_SAKOECHIBA_HPP_
#define POLICY_SAKOECHIBA_HPP_

#include "../iterators/step_iterator.hpp"
#include "../util/math.hpp"

#include <cmath>        // for std::abs

template<template<typename> class StepPolicy, typename Matrix>
struct SakoeChiba
{
public:
  typedef SakoeChiba this_type;
  typedef std::size_t size_type;
  typedef StepPolicy<SakoeChiba> step_policy;
  typedef typename step_policy::step_type step_type;
  typedef step_iterator<this_type> iterator;
  typedef reverse_step_iterator<this_type> reverse_iterator;

public:
  SakoeChiba(size_type size, const Matrix& matrix)
  : matrix_(matrix)
  , size_(size / 2.0)
  , ratio_((double)matrix_.rows() / (double)matrix_.columns())
  { }

  iterator begin()
  {
    return iterator(*this, step_policy::begin(matrix_));
  }

  iterator end()
  {
    return iterator(*this, step_policy::end(matrix_));
  }

  reverse_iterator rbegin()
  {
    return reverse_iterator(*this, step_policy::rbegin(matrix_));
  }

  reverse_iterator rend()
  {
    return reverse_iterator(*this, step_policy::rend(matrix_));
  }

  step_type next(const step_type& cur) const
  {
    const step_type next = step_policy::next(cur, matrix_, *this);
    if(step_policy::validate(next)) {
      return step_type(cur.first + next.first, cur.second + next.second);
    }
    return step_policy::end(matrix_);
  }

  step_type prev(const step_type& cur) const
  {
    const step_type prev = step_policy::prev(cur, matrix_, *this);
    if(step_policy::validate(prev)) {
      return step_type(cur.first + prev.first, cur.second + prev.second);
    }
    return step_policy::rend(matrix_);
  }

  bool validate(const step_type& cur, const step_type& next) const
  {
    const double middle = ratio_ * next.second;
    return math::lt(next.first, matrix_.rows()) &&
           math::lt(next.second, matrix_.columns()) &&
           math::lte(std::abs(middle - next.first), size_);
  }

private:
  const Matrix& matrix_;
  double size_;
  double ratio_;
};

template<template<typename> class StepPolicy, typename Matrix>
SakoeChiba<StepPolicy, Matrix> make_sakoechiba(std::size_t n, const Matrix& matrix)
{
  return SakoeChiba<StepPolicy, Matrix>(n, matrix);
}

#endif
