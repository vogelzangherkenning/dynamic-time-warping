# Environment
MKDIR = mkdir
CP    = cp
GREP  = grep
NM    = nm
AS    = as

# Compiler flags
LDFLAGS   =
CCFLAGS   = -c -std=c++0x -Wall -Werror -pedantic-errors -O3 -fPIC
CXXFLAGS  = -c -std=c++11 -Wall -Werror -pedantic-errors -O3 -fPIC

CCFLAGS_DEBUG = -c -std=c++0x -Wall -Werror -pedantic-errors -g
CXXFLAGS_DEBUG  = -c -std=c++11 -Wall -Werror -pedantic-errors -g

# Directories
BINDIR  = bin/
SRCDIR  =

DIR_GUARD = @mkdir -p $(@D)

# Helpers
EMPTY :=
SPACE := $(EMPTY) $(EMPTY)

FILES = dtw.cpp

SOURCES = $(SRCDIR)$(subst $(SPACE), $(SRCDIR),$(FILES))
OBJECTS = $(subst .cpp,.o,$(BINDIR)$(subst $(SPACE), $(BINDIR),$(FILES)))
SOURCE  = $(subst .o,.cpp,$(subst $(BINDIR),$(SRCDIR),$(1)))

EXE_PREFIX  =
EXE_POSTFIX =
EXE_BASE    = dtw
EXECUTABLE  = $(EXE_PREFIX)$(EXE_BASE)$(EXE_POSTFIX)

# Stages

all: $(EXE_BASE)

release: $(EXE_BASE)

$(EXE_BASE): $(OBJECTS)
	$(DIR_GUARD)
	$(CXX) $(LDFLAGS) $(OBJECTS) -o $(EXECUTABLE)

$(OBJECTS):
	$(DIR_GUARD)
	$(CXX) $(CXXFLAGS) $(call SOURCE,$@) -o $@

debug: CXXFLAGS = $(CXXFLAGS_DEBUG)
debug: CCFLAGS = $(CCFLAGS_DEBUG)
debug: $(EXE_BASE)

.PHONY: clean
clean:
	rm -rf $(BINDIR)
