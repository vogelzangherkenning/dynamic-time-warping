#include <node.h>
#include <v8.h>
#include <vector>
#include "include/algo.hpp"
#include "include/dtw.hpp"
#include "include/policies.hpp"
#include "include/printers.hpp"
#include "include/reducers.hpp"
#include "include/util.hpp"

#include "include/node/helpers.hpp"

using namespace v8;

// Wrapper arround dtw::normalize
Handle<Value> Normalize(const Arguments& args) {
	HandleScope scope;

	if( args.Length() != 1 ) {
		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
		return scope.Close(Undefined());
	}

	if( !args[0]->IsArray() ) {
		ThrowException(Exception::TypeError(String::New("Argument should be an array")));
		return scope.Close(Undefined());
	}

	Local<Array> data = Array::Cast(*args[0]);

	std::vector<double> vector = arrayToDoubleVector(data);

	vector = dtw::normalize(vector);

	return scope.Close(doubleVectorToArray(vector));
}

// PAA
Handle<Value> ReducePaa(const Arguments& args) {
	HandleScope scope;

	if( args.Length() != 2 ) {
		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
		return scope.Close(Undefined());
	}

	// args[0] is the spectogram
	if( !args[0]->IsArray() ) {
		ThrowException(Exception::TypeError(String::New("Argument 1 should be an array")));
		return scope.Close(Undefined());
	}

	// args[1] is the number of dimensions
	if( !args[1]->IsNumber() ) {
		ThrowException(Exception::TypeError(String::New("Argument 2 should be an integer")));
		return scope.Close(Undefined());
	}

	Local<Array> data = Array::Cast(*args[0]);
	double dimensions = args[1]->NumberValue();

	std::vector<double> vector = arrayToDoubleVector(data);

	vector = dtw::reduce( vector, make_paa((int)dimensions, vector) );

	return scope.Close(doubleVectorToArray(vector));
}

//Get path cost warping
Handle<Value> GetWarpingpath(const Arguments& args) {
	HandleScope scope;

	if( args.Length() != 3) {
		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
		return scope.Close(Undefined());
	}

	// args[0] is q
	if( !args[0]->IsArray() ) {
		ThrowException(Exception::TypeError(String::New("Argument 1 should be an array")));
		return scope.Close(Undefined());
	}

	// args[0] is c
	if( !args[1]->IsArray() ) {
		ThrowException(Exception::TypeError(String::New("Argument 2 should be an array")));
		return scope.Close(Undefined());
	}

	// args[1] is bandWidth
	if( !args[2]->IsNumber()) {
		ThrowException(Exception::TypeError(String::New("Argument 3 should be an integer")));
		return scope.Close(Undefined());
	}

	Local<Array> dataQ = Array::Cast(*args[0]);
	Local<Array> dataC = Array::Cast(*args[1]);
	double bandWidth = args[2]->NumberValue();

	std::vector<double> vectorQ = arrayToDoubleVector(dataQ);
	std::vector<double> vectorC = arrayToDoubleVector(dataC);

	//Calculate our wrapping matrix
	dtw::warpingmatrix<double> mat = dtw::get_warpingmatrix(vectorQ, vectorC);

	//Get warping path using Sakoe-Chiba band with
	dtw::warpingpath path = dtw::get_warpingpath(mat, make_sakoechiba<EastSouthEast>((int) bandWidth, mat));

	//Calculate path cost
	int path_cost = dtw::get_path_cost(path, mat);

	Local<Number> num = Number::New(path_cost);
	return scope.Close(num);
}

void init(Handle<Object> exports) {
	// Export Normalize
	exports->Set( String::NewSymbol("normalize"),
		FunctionTemplate::New(Normalize)->GetFunction() );

	exports->Set( String::NewSymbol("reduce"),
		FunctionTemplate::New(ReducePaa)->GetFunction() );

	exports->Set( String::NewSymbol("pathCost"),
		FunctionTemplate::New(GetWarpingpath)->GetFunction() );
}

NODE_MODULE(warping, init)